#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    ifstream Openfile(inputFilePath); //Apertura del file e check sull'importo corretto
    char ch;
    bool resultImport;
    while(!Openfile.eof())
      {
        Openfile.get(ch);
        text.push_back(ch);
      }
    Openfile.close();
    if (text.size() > 0)
      resultImport = true;
    else
      resultImport = false;

    return resultImport;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
  string textNumber, passwordNumber, repeatPassword,encryptedTextNumber;
  bool resultEncryption;

  for (unsigned int i=0; i < text.size(); i++) //Testo in codice ASCII
    textNumber.push_back((int)(text[i]));

  for (unsigned int i=0; i < password.size(); i++) //Password in codice ASCII
    passwordNumber.push_back((int)(password[i]));

  for (unsigned int i=0; i < text.size(); i++) //Password ripetuta per la lunghezza del testo
    repeatPassword.push_back(passwordNumber[i % password.size()]);

  for (unsigned int i=0; i < text.size(); i++) //Criptaggio
    encryptedTextNumber.push_back(repeatPassword[i] + textNumber[i]);

  for (unsigned int i=0; i < encryptedTextNumber.size(); i++) //Da ASCII
      encryptedText.push_back((char)(encryptedTextNumber[i]));

  if (encryptedText.size() == text.size()) //Check
    resultEncryption = true;
  else
    resultEncryption = false;

  return resultEncryption;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
  string textNumber, passwordNumber, repeatPassword,dencryptedTextNumber;
  bool resultEncryption;

  for (unsigned int i=0; i < text.size(); i++) //Testo in ASCII
    textNumber.push_back((int)(text[i]));

  for (unsigned int i=0; i < password.size(); i++) //Password in ASCII
    passwordNumber.push_back((int)(password[i]));

  for (unsigned int i=0; i < text.size(); i++) //Password ripetuta
    repeatPassword.push_back(passwordNumber[i % password.size()]);

  for (unsigned int i=0; i < text.size(); i++) //Decriptaggio
    dencryptedTextNumber.push_back(textNumber[i] - repeatPassword[i]);

  for (unsigned int i=0; i < dencryptedTextNumber.size(); i++) //Da ASCII
      decryptedText.push_back((char)(dencryptedTextNumber[i]));

  if (decryptedText.size() == text.size()) //Check
    resultEncryption = true;
  else
    resultEncryption = false;

  return resultEncryption;
}
