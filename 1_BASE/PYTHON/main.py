import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text


def import_text(input_file_path):
    read = open(input_file_path, "r").read()  # Lettura da file
    if len(read) > 0:  # Check corretta lettura da file
        result_import = True
    else:
        result_import = False
    return result_import, read

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text


def encrypt(text, password):
    text_number, password_number, repeat_password, encrypted_text_number, encrypted_text = [], [], [], [], []

    for i in text:  # Testo in ASCII
        text_number.append(int(str(ord(i))))

    for i in password:  # Password in ASCII
        password_number.append(int(str(ord(i))))

    for i in range(0, len(text)):  # Password ripetuta
        repeat_password.append(password_number[i % len(password)])

    for i in range(0, len(text)):  # Criptaggio
        encrypted_text_number.append(repeat_password[i] + text_number[i])

    for i in encrypted_text_number:  # Da ASCII
        encrypted_text.append((str(chr(i))))

    result = "".join(encrypted_text)  # Il join() prende tutti gli elementi in un iterabile e li unisce in una stringa

    if len(result) == len(text):  # Check
        result_encrypt = True
    else:
        result_encrypt = False

    return result_encrypt, result

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text


def decrypt(text, password):
    text_number, password_number, repeat_password, dencrypted_text_number, dencrypted_text = [], [], [], [], []

    for i in text:  # Testo in ASCII
        text_number.append(int(str(ord(i))))

    for i in password:  # Password in ASCII
        password_number.append(int(str(ord(i))))

    for i in range(0, len(text)):  # Ripetizione password
        repeat_password.append(password_number[i % len(password)])

    for i in range(0, len(text)):  # Decriptaggio
        dencrypted_text_number.append(text_number[i] - repeat_password[i])

    for i in dencrypted_text_number:  # Da ASCII
        dencrypted_text.append((str(chr(i))))

    result = "".join(dencrypted_text)  # Il join() prende tutti gli elementi in un iterabile e li unisce in una stringa

    if len(result) == len(text):  # Check
        result_encrypt = True
    else:
        result_encrypt = False

    return result_encrypt, result


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    input_file_path = "text.txt"

    [resultImport, text] = import_text(input_file_path)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
