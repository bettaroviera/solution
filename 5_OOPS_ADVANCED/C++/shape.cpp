#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Point Point::operator+(const Point &point) const
  {
    Point pointPlus(X, Y);
    pointPlus.X = X + point.X;
    pointPlus.Y = Y + point.Y;
    return pointPlus;
  }

  Point Point::operator-(const Point &point) const
  {
    Point pointMinus(X, Y);
    pointMinus.X = X - point.X;
    pointMinus.Y = Y - point.Y;
    return pointMinus;
  }

  Point &Point::operator-=(const Point &point)
  {
    X -= point.X;
    Y -= point.Y;
    return *this;
  }

  Point &Point::operator+=(const Point &point)
  {
    X += point.X;
    Y += point.Y;
    return *this;
  }

  ostream& operator<<(ostream& stream, const Point& point)
  {
    stream << "Point: x = " << point.X << " y = " << point.Y << endl;
    return stream;
  }

  Ellipse::Ellipse(const Point &center, const double &a, const double &b)
  {
    _center = center;
    _a = a;
    _b = b;
  }

  Circle::Circle(const Point &center, const double &radius) : Ellipse(center, radius, radius) { }

  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  { 
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _edge = 0;
  }

  void Triangle::AddVertex(const Point &point)
  {
    if (points.size() < 3)
      points.push_back(point);
    else
      throw runtime_error("The triangle already has three vertices");
  }

   double Triangle::Perimeter() const
   {
     double edge1, edge2, edge3, perimeter;
     Point point;
     if (_edge == 0)
       {
         edge1 = point.ComputeDistance(_p1, _p2);
         edge2 = point.ComputeDistance(_p2, _p3);
         edge3 = point.ComputeDistance(_p3, _p1);
       }
     else
       edge1 = edge2 = edge3 = _edge;
     perimeter = edge1 + edge2 + edge3;
     return perimeter;
   }

   TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge) { _edge = edge; }

   Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
   {
     _p1 = p1;
     _p2 = p2;
     _p3 = p3;
     _p4 = p4;
     _edge1 = 0;
     _edge2 = 0;
   }

   void Quadrilateral::AddVertex(const Point &p)
   {
     if (points.size() < 4)
       points.push_back(p);
     else
       throw runtime_error("The quadrilateral already has four vertices");
   }

   double Quadrilateral::Perimeter() const
   {
     double edge1, edge2, edge3, edge4, perimeter;
     Point point;
     if (_edge1 == 0)
       {
         edge1 = point.ComputeDistance(_p1, _p2);
         edge2 = point.ComputeDistance(_p2, _p3);
         edge3 = point.ComputeDistance(_p3, _p4);
         edge4 = point.ComputeDistance(_p4, _p1);
       }
     else
       {
         edge1 = edge3 = _edge1;
         edge2 = edge4 = _edge2;
       }
     perimeter = edge1 + edge2 + edge3 + edge4;
     return perimeter;
   }

   Rectangle::Rectangle(const Point& p1, const double& base, const double& height)
   {
     _edge1 = base;
     _edge2 = height;
   }

   Square::Square(const Point &p1, const double &edge)
   {
     _edge1 = edge;
     _edge2 = edge;
   }

}
