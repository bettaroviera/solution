#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X = 0.0; Y = 0.0; };
      Point(const double& x,
            const double& y) { X = x; Y = y;}
      Point(const Point& point) { *this = point; }
      double ComputeNorm2() const { return sqrt(pow(X, 2) + pow(Y, 2));}
      double ComputeDistance(const Point& A, const Point& B) const { return sqrt(pow((B.X - A.X), 2) + pow((B.Y - A.Y), 2)); }

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point);
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs) { return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs) { return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs) { return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs) { return lhs.Perimeter() >= rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { }
      double Perimeter() const { return 2 * M_PI * sqrt((pow((_a), 2) + pow((_b), 2)) / 2); }
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { }
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;
      Point _p1, _p2, _p3;
      double _edge;

    public:
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);
      void AddVertex(const Point& point);
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;
      Point _p1, _p2, _p3, _p4;
      double _edge1, _edge2;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p);

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {}
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
