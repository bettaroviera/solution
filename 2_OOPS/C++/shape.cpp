#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Point::Point(const double &x, const double &y)
  {
    _x = x;
    _y = y;
  }

  Point::Point(const Point &point)
  {
    _x = point._x;
    _y = point._y;
  }

  double Point::Distance(const Point &A, const Point &B) { return sqrt(pow((A._x - B._x), 2) + pow((A._y - B._y), 2)); }

  Ellipse::Ellipse(const Point& center,const int& a, const int& b)
  {
    _a = a;
    _b = b;
  }

  double Ellipse::Area() const { return M_PI * _a * _b; }

  Circle::Circle(const Point& center, const int& radius) : Ellipse(center, radius, radius) { }

  double Circle::Area() const { return Ellipse::Area(); }

  Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
  {
    Point point;
    _edge1 = point.Distance(p1, p2);
    _edge2 = point.Distance(p2, p3);
    _edge3 = point.Distance(p3, p1);
    _semip = (_edge1 + _edge2 + _edge3) * 0.5;
  }

  double Triangle::Area() const { return sqrt(_semip * (_semip - _edge1) * (_semip - _edge2) * (_semip - _edge3)); }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const int& edge) : Triangle(p1, p1, p1)
  {
    _edge1 = _edge2 = _edge3 = edge;
    _semip = (_edge1 + _edge2 + _edge3) * 0.5;
  }

  double TriangleEquilateral::Area() const { return Triangle::Area(); }

  Quadrilateral::Quadrilateral(const Point& p1, const Point& p2, const Point& p3, const Point& p4)
  {
    Point point;
    _edge1 = point.Distance(p1, p2);
    _edge2 = point.Distance(p2, p3);
    _edge3 = point.Distance(p3, p4);
    _edge4 = point.Distance(p4, p1);
    _diagonal = point.Distance(p1, p3);
    _semip1 = (_edge1 + _edge2 + _diagonal) * 0.5;
    _semip2 = (_edge3 + _edge4 + _diagonal) * 0.5;
  }

  double Quadrilateral::Area() const
  {
    double area1, area2;
    area1 = sqrt(_semip1 * (_semip1 - _edge1) * (_semip1 - _edge2) * (_semip1 -  _diagonal));
    area2 = sqrt(_semip2 * (_semip2 - _edge3) * (_semip2 - _edge4) * (_semip2 -  _diagonal));
    return area1 + area2;
  }

  Parallelogram::Parallelogram(const Point& p1, const Point& p2, const Point& p4) : Quadrilateral(p1, p2, p2, p4)
  {
    Point point;
    _edge1 = _edge3 = point.Distance(p1, p2);
    _edge4 = _edge2 = point.Distance(p4, p1);
    _diagonal = point.Distance(p2, p4);
    _semip1 = (_edge1 + _edge4 + _diagonal) * 0.5;
    _semip2 = _semip1;
  }

  double Parallelogram::Area() const { return Quadrilateral::Area(); }

  Rectangle::Rectangle(const Point& p1, const int& base, const int& height) : Parallelogram(p1, p1, p1)
  {
    _base = base;
    _height = height;
    _edge1 = _edge3 = _base;
    _edge4 = _edge2 =_height;
    _diagonal = sqrt(pow(_base, 2) + pow(_height, 2));
    _semip1 = (_edge1 + _edge4 + _diagonal) * 0.5;
    _semip2 = _semip1;
  }

  double Rectangle::Area() const { return Parallelogram::Area(); }

  Square::Square(const Point& p1, const int& edge) : Rectangle(p1, edge, edge)
  {
    _edge1 = _edge2 = _edge3 = _edge4 = _edge = edge;
    _semip1 = (_edge + _edge + _diagonal) * 0.5;
    _semip2 = _semip1;
  }

  double Square::Area() const { return Rectangle::Area(); }

}

