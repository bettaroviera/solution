#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point
  {
    protected:
      double _x, _y;

    public:
        Point(const double& x,
              const double& y);
        Point(const Point& point);
        Point () {}
        double Distance(const Point& A, const Point& B);

  };

  class IPolygon {
    public:
        virtual double Area() const = 0;

  };

  class Ellipse : public IPolygon
  {
    protected:
      int _a, _b;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);
      double Area() const;

  };

  class Circle : public Ellipse
  {
    protected:
      int _radius;
    public:
      Circle(const Point& center,
             const int& radius);
      double Area() const;
  };


  class Triangle : public IPolygon
  {
    protected:
      double _edge1, _edge2, _edge3, _semip;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);
      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    protected:
      int _edge;
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);
      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      double _edge1, _edge2, _edge3, _edge4, _diagonal, _semip1, _semip2;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);
      double Area() const;
  };

  class Rectangle : public Parallelogram
  {
    protected:
      int _base, _height;
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);
      double Area() const;
  };

  class Square: public Rectangle
  {
    protected:
      int _edge;
    public:
      Square(const Point& p1,
             const int& edge);
      double Area() const;
  };
}

#endif // SHAPE_H
