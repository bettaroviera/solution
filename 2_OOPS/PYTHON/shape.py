import math as math


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class Edge:
    def __init__(self, a: Point, b: Point):
        self.A = a
        self.B = b

    def length(self) -> float:
        return abs(math.sqrt(pow((self.A.x - self.B.x), 2) + pow((self.A.y - self.B.y), 2)))


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.a = a
        self.b = b

    def area(self) -> float:
        return math.pi * self.a * self.b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.edge1 = Edge.length(Edge(p1, p2))
        self.edge2 = Edge.length(Edge(p2, p3))
        self.edge3 = Edge.length(Edge(p1, p3))

    def area(self) -> float:
        semi_p = (self.edge1 + self.edge2 + self.edge3) * 0.5
        return math.sqrt(semi_p * (semi_p - self.edge1) * (semi_p - self.edge2) * (semi_p - self.edge3))


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, p1, p1)
        self.edge1 = edge
        self.edge2 = edge
        self.edge3 = edge
        Triangle.area(self)


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.edge1 = Edge.length(Edge(p1, p2))
        self.edge2 = Edge.length(Edge(p2, p3))
        self.edge3 = Edge.length(Edge(p3, p4))
        self.edge4 = Edge.length(Edge(p1, p4))
        self.diagonal = Edge.length(Edge(p1, p3))

    def area(self) -> float:
        semi_p1 = (self.edge1 + self.edge2 + self.diagonal) * 0.5
        semi_p2 = (self.diagonal + self.edge3 + self.edge4) * 0.5
        area_1 = math.sqrt(semi_p1 * (semi_p1 - self.edge1) * (semi_p1 - self.edge2) * (semi_p1 - self.diagonal))
        area_2 = math.sqrt(semi_p2 * (semi_p2 - self.diagonal) * (semi_p2 - self.edge3) * (semi_p2 - self.edge4))
        return area_1 + area_2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, p2, p4)
        self.edge1 = Edge.length(Edge(p1, p2))
        self.edge2 = Edge.length(Edge(p1, p4))
        self.edge3 = Edge.length(Edge(p1, p2))
        self.edge4 = Edge.length(Edge(p1, p4))
        self.diagonal = Edge.length(Edge(p2, p4))
        Quadrilateral.area(self)


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__(p1, p1, p1)
        self.edge1 = base
        self.edge2 = height
        self.edge3 = base
        self.edge4 = height
        self.diagonal = math.sqrt(pow(base, 2) + pow(height, 2))
        Parallelogram.area(self)


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
        Rectangle.area(self)
