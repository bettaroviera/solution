from unittest import TestCase
import shape as shape_library


class TestEllipse(TestCase):
    def test_area(self):
        self.assertTrue(
            abs(shape_library.Ellipse(shape_library.Point(1.2, 2.5), 12, 15).area() - 565.4866776461628) < 1e-6)


class TestCircle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Circle(shape_library.Point(1.2, 2.5), 17).area() - 907.9202768874503) < 1e-6)


class TestTriangle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Triangle(shape_library.Point(2.8, 3.9), shape_library.Point(3.9, 1.8),
                                                   shape_library.Point(3.1, 2.7)).area() - 0.345) < 1e-6)


class TestTriangleEquilateral(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.TriangleEquilateral(shape_library.Point(1.2, 1.7), 16).area() - 110.851251684)
                        < 1e-6)


class TestQuadrilateral(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Quadrilateral(shape_library.Point(-3, 1), shape_library.Point(1, -2),
                                                        shape_library.Point(3, 2),
                                                        shape_library.Point(-1, 4)).area() - 19.0) < 1e-6)


class TestParallelogram(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Parallelogram(shape_library.Point(3, 1),
                                                        shape_library.Point(10, 1),
                                                        shape_library.Point(4, 3)).area() - 14.0) < 1e-6)


class TestRectangle(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Rectangle(shape_library.Point(3, 1), 7, 3).area() - 21.0) < 1e-6)


class TestSquare(TestCase):
    def test_area(self):
        self.assertTrue(abs(shape_library.Square(shape_library.Point(3, 1), 7).area() - 49.0) < 1e-6)
