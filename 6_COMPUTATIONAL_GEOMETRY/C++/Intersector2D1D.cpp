#include "Intersector2D1D.h"

Intersector2D1D::Intersector2D1D()
{
  toleranceParallelism = 1.0E-7;
  toleranceIntersection = 1.0E-7;
  intersectionType = Intersector2D1D::NoInteresection;
}

void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  _planeNormal = planeNormal;
  _planeTranslation = planeTranslation;
}

void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  _lineOrigin = lineOrigin;
  _lineTangent = lineTangent;
}

Vector3d Intersector2D1D::IntersectionPoint()
{
  double dot1 = 0, dot2 = 0;
  for (unsigned int i = 0; i < _planeNormal.size(); i++) //Prodotto scalare
    dot1 += _planeNormal(i) * _lineOrigin(i);
  for (int i = 0; i < _planeNormal.size(); i++) //Prodotto scalare
    dot2 += _planeNormal(i) * _lineTangent(i);
  intersectionParametricCoordinate = (_planeTranslation - dot1) / dot2; //s = (d - N * x0) / N * t

  Vector3d intersectionPoint, flag;
  for (int i = 0; i < _planeNormal.size(); i++)
    flag(i) = intersectionParametricCoordinate * _lineTangent(i);
  for (int i = 0; i < _planeNormal.size(); i++)
    intersectionPoint(i) = _lineOrigin(i) + flag(i);
  return intersectionPoint;
}

bool Intersector2D1D::ComputeIntersection()
{
  intersectionType = NoInteresection;
  bool intersection = false;
  double check = 0; //check = N * t
  for (int i = 0; i < _planeNormal.size(); i++) //Prodotto scalare
    check += _planeNormal(i) * _lineTangent(i);
  if ((abs(check) > toleranceIntersection))
    {
      intersectionType = PointIntersection;
      intersection = true;
    }
  int dot = 0;
  for (int i = 0; i < _planeNormal.size(); i++) //Prodotto scalare
    dot += _planeNormal(i) * _lineOrigin(i);
  if ((abs(check) < toleranceParallelism))
    {
      if (_planeTranslation == dot)
        intersectionType = Coplanar;
      else
        intersectionType = NoInteresection;
    }
  return intersection;
}
