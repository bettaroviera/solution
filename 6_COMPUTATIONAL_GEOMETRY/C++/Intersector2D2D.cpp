#include "Intersector2D2D.h"

Intersector2D2D::Intersector2D2D()
{
  toleranceParallelism = 1.0E-5;
  toleranceIntersection = 1.0E-7;
  intersectionType = Intersector2D2D::NoInteresection;
}

void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  rightHandSide(0) = planeTranslation;
  for (unsigned int i = 0; i < planeNormal.size();  i++)
      matrixNomalVector(0, i) = planeNormal(i);
}

void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  rightHandSide(1) = planeTranslation;
  for (unsigned int i = 0; i < planeNormal.size();  i++)
      matrixNomalVector(1, i) = planeNormal(i);
}


bool Intersector2D2D::ComputeIntersection()
{
  bool intersection = false;
  //Prodotto Vettoriale v * w = (v2w3 - v3w2)i + (v3w1 - v1w3)j + (v1w2 - v2w1)k
  matrixNomalVector(2, 0) = matrixNomalVector(0, 1) * matrixNomalVector(1, 2) - matrixNomalVector(0, 2) * matrixNomalVector(1, 1);
  matrixNomalVector(2, 1) = matrixNomalVector(0, 2) * matrixNomalVector(1, 0) - matrixNomalVector(0, 0) * matrixNomalVector(1, 2);
  matrixNomalVector(2, 2) = matrixNomalVector(0, 0) * matrixNomalVector(1, 1) - matrixNomalVector(0, 1) * matrixNomalVector(1, 0);
  if (matrixNomalVector.row(2).squaredNorm() > toleranceIntersection)
    {
      intersectionType = LineIntersection;
      intersection = true;
    }
  if (matrixNomalVector.row(2).squaredNorm() < toleranceIntersection)
    {
      if (matrixNomalVector.row(0).squaredNorm() - matrixNomalVector.row(1).squaredNorm() < toleranceParallelism && rightHandSide(0) == rightHandSide(1))
        intersectionType = Coplanar;
      else
        intersectionType = NoInteresection;
    }
  return intersection;
}
