#ifndef POL_H
#define POL_H

#include <iostream>

#include <Eigen>
#include <vector>

using namespace Eigen;
using namespace std;


namespace PolLibrary {

  class INewPoints {
    public:
      virtual vector<Vector2d> OrderedVertex() = 0;
      virtual void CalcPoints() = 0;
      virtual vector<Vector2d> ReturnPoints() const = 0;
      virtual int NumNewPoints() const = 0;
  };

  class ISegment {
    public:
      virtual double Distance() const = 0;
      virtual Vector2d Direction() const = 0;
  };

  class ICutPolygon {
    public:
      virtual void Cut() = 0;
      virtual int& NumIntersections() = 0;
      virtual vector<Vector2d> IntersectionPoints() const = 0;
      virtual void ChooseFunction() = 0;
      virtual void NewPolygonsTwoInt() = 0;
      virtual void NewPolygonsMoreInt() = 0;
      virtual vector<vector<Vector2d>> CuttedPolygons() = 0;
      virtual vector<vector<int>> NewPolygonsVertex() = 0;
  };

  class IIntersector {
    public:
      virtual void SetStraight(const Vector2d& origin1, const Vector2d& end1) = 0;
      virtual void SetEdge(const Vector2d& origin2, const Vector2d& end2) = 0;
      virtual bool Intersection() = 0;
      virtual const Vector2d& IntersectionCoordinates() = 0;
      virtual const Vector2d& ParametricCoordinates() const = 0;
  };

  class IPolygon {
    public:
      virtual double ComputeArea() = 0;
      virtual vector<Vector2d> ComputeBoundingBox() = 0;
  };

  class IReferenceElement {
    public:
      virtual vector<Vector2d> NewBoundingBox() = 0;
      virtual vector<Vector2d> CreateRefElement() = 0;
      virtual vector<Vector2d> Translate(vector<Vector2d> element, Vector2d translation) = 0;
      virtual vector<vector<Vector2d>> CreateMesh(vector<Vector2d> domain) = 0;
  };

}

#endif // POL_H

//Implemento tutte le interfacce con le funzioni virtuali per realizzare la DI
