#ifndef REFERENCEELEMENT_H
#define REFERENCEELEMENT_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Segment.hpp>
#include <NewPoints.hpp>
#include <Eigen>
#include <Polygon.hpp>
#include <CutPolygon.hpp>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class ReferenceElement : public IReferenceElement {

  private:
      vector<Vector2d> _polygon, _boundingBox, _newBB, _transElement, _refElement;
      vector<vector<Vector2d>> _mesh, _ref;
      double _baseBB, _heightBB, _areaTot;

      void Reset();

  public:

      ReferenceElement(vector<Vector2d> polygon, vector<Vector2d> boundingBox);

      vector<Vector2d> NewBoundingBox(); //Aggiunge i punti per formare una mesh
      vector<Vector2d> CreateRefElement(); //Crea l'elemento con i vertici del poligono e poi i vertici della BB
      vector<Vector2d> Translate(vector<Vector2d> element, Vector2d translation); //Trasla la BB
      vector<vector<Vector2d>> CreateMesh(vector<Vector2d> domain); //Forma la mesh
      double AreaTot(); //Calcola l'area generale di tutti i poligoni dentro il dominio

  };
}

#endif // REFERENCEELEMENT_H
