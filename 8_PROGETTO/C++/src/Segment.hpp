#ifndef SEGMENT_H
#define SEGMENT_H

#include <iostream>
#include <Eigen>
#include <vector>

#include <Pol.hpp>

using namespace Eigen;
using namespace std;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class Segment: public ISegment {
    private:
      Vector2d _s1, _s2;

      void Reset() {};

    public:
      Segment(const Vector2d s1, Vector2d s2) {_s1 = s1;
                                               _s2 = s2;}

      double Distance () const; //Restituisce la lunghezza del segmento: la distanza tra s1 e s2
      Vector2d Direction() const; //Restituisce il vettore che dà la direzione del segmento: s2 - s1
  };
}

#endif // SEGMENT_H
