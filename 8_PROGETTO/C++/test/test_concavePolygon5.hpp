#ifndef __TEST_CONCAVEPOLYGON5_H
#define __TEST_CONCAVEPOLYGON5_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConcavePolygon5Testing {

  TEST(TestPolygonCut, TestConcavePolygon5)
  {
      Vector2d v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;
      v1 << 2.0, -2.0;
      v2 << 0.0, -1.0;
      v3 << 3.0, 1.0;
      v4 << 0.0, 2.0;
      v5 << 3.0, 2.0;
      v6 << 3.0, 3.0;
      v7 << -1.0, 3.0;
      v8 << -3.0, 1.0;
      v9 << 0.0, 0.0;
      v10 << -3.0, -2.0;


      Vector2d s1, s2; //li ho scelti io perché i suoi erano fuori dal poligono
      s1 << 0, -2;
      s2 << 0, 3;

      vector<Vector2d> concavePolygon5Vertex; //nell'ordine in cui li ricevo
      concavePolygon5Vertex.push_back(v1);
      concavePolygon5Vertex.push_back(v2);
      concavePolygon5Vertex.push_back(v3);
      concavePolygon5Vertex.push_back(v4);
      concavePolygon5Vertex.push_back(v5);
      concavePolygon5Vertex.push_back(v6);
      concavePolygon5Vertex.push_back(v7);
      concavePolygon5Vertex.push_back(v8);
      concavePolygon5Vertex.push_back(v9);
      concavePolygon5Vertex.push_back(v10);

      try
      {
        PolygonCutLibrary::CutPolygon(concavePolygon5Vertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon concavePolygon5(concavePolygon5Vertex, s1, s2);

      try
      {
        concavePolygon5.Cut();
        EXPECT_EQ(5, concavePolygon5.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, inters3, inters4, inters5, a, b, c, d, e;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 0, -2;
      inters2 << 0, -1;
      inters3 << 0, 2;
      inters4 << 0, 3;
      inters5 << 0, 0;

      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon5.IntersectionPoints()[0];
        b = concavePolygon5.IntersectionPoints()[1];
        c = concavePolygon5.IntersectionPoints()[2];
        d = concavePolygon5.IntersectionPoints()[3];
        e = concavePolygon5.IntersectionPoints()[4];
        intersectionPoints = concavePolygon5.IntersectionPoints();
        //controllo che coincidano
        EXPECT_EQ(a, inters1);
        EXPECT_EQ(b, inters2);
        EXPECT_EQ(c, inters3);
        EXPECT_EQ(d, inters4);
        EXPECT_EQ(e, inters5);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
         PolygonCutLibrary::NewPoints(concavePolygon5Vertex, intersectionPoints, s1, s2);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: NewPoints newP(concavePolygon5Vertex, intersectionPoints, s1, s2);

      vector<Vector2d> newPoints;
      try
      {
        newP.CalcPoints();
        EXPECT_EQ(12, newP.NumNewPoints());
        newPoints = newP.ReturnPoints();
        // controllo che i primi 10 punti corrispondano ai vertici
        EXPECT_EQ(v10, newPoints[0]);
        EXPECT_EQ(v1, newPoints[1]);
        EXPECT_EQ(v2, newPoints[2]);
        EXPECT_EQ(v3, newPoints[3]);
        EXPECT_EQ(v4, newPoints[4]);
        EXPECT_EQ(v5, newPoints[5]);
        EXPECT_EQ(v6, newPoints[6]);
        EXPECT_EQ(v7, newPoints[7]);
        EXPECT_EQ(v8, newPoints[8]);
        EXPECT_EQ(v9, newPoints[9]);
        // controllo che 2 siano le intersezioni non coincidenti con i vertici
        EXPECT_EQ(newPoints[10], inters1);
        EXPECT_EQ(newPoints[11], inters4);

        concavePolygon5.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2, cuttedPolygon3, cuttedPolygon4, cuttedPolygon5;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2, newVertex3, newVertex4, newVertex5;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v10);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(v9);
      newVertex1.push_back(0);
      newVertex1.push_back(10);
      newVertex1.push_back(2);
      newVertex1.push_back(9);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v1);
      cuttedPolygon2.push_back(v2);
      newVertex2.push_back(10);
      newVertex2.push_back(1);
      newVertex2.push_back(2);

      cuttedPolygon3.push_back(b);
      cuttedPolygon3.push_back(v3);
      cuttedPolygon3.push_back(v4);
      cuttedPolygon3.push_back(v9);
      newVertex3.push_back(2);
      newVertex3.push_back(3);
      newVertex3.push_back(4);
      newVertex3.push_back(9);

      cuttedPolygon4.push_back(c);
      cuttedPolygon4.push_back(v5);
      cuttedPolygon4.push_back(v6);
      cuttedPolygon4.push_back(d);
      newVertex4.push_back(4);
      newVertex4.push_back(5);
      newVertex4.push_back(6);
      newVertex4.push_back(11);

      cuttedPolygon5.push_back(d);
      cuttedPolygon5.push_back(v7);
      cuttedPolygon5.push_back(v8);
      cuttedPolygon5.push_back(v9);
      cuttedPolygon5.push_back(v4);
      newVertex5.push_back(11);
      newVertex5.push_back(7);
      newVertex5.push_back(8);
      newVertex5.push_back(9);
      newVertex5.push_back(4);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allCuttedPol.push_back(cuttedPolygon3);
      allCuttedPol.push_back(cuttedPolygon4);
      allCuttedPol.push_back(cuttedPolygon5);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);
      allNewVertex.push_back(newVertex3);
      allNewVertex.push_back(newVertex4);
      allNewVertex.push_back(newVertex5);

      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon5.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon5.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }
  }
}

#endif // __TEST_CONCAVEPOLYGON5_H
