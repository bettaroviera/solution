#ifndef __TEST_POLYGON_H
#define __TEST_POLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace PolygonTesting {

  TEST (TestPolygonCut, PolygonTest)
  {
      Vector2d v1, v2, v3, v4, v5, v6;
      v1 << 1.5, 1.0;
      v2 << 5.6, 1.5;
      v3 << 5.5, 4.8;
      v4 << 4.0, 6.2;
      v5 << 3.2, 4.2;
      v6 << 1.0, 4.0;

      vector<Vector2d> polygonVertex;
      polygonVertex.push_back(v1);
      polygonVertex.push_back(v2);
      polygonVertex.push_back(v3);
      polygonVertex.push_back(v4);
      polygonVertex.push_back(v5);
      polygonVertex.push_back(v6);

      Vector2d p1, p2, p3, p4;
      p1 << 1, 1;
      p2 << 5.6, 1;
      p3 << 5.6, 6.2;
      p4 << 1, 6.2;
      vector<Vector2d> boundingBox;
      boundingBox.push_back(p1);
      boundingBox.push_back(p2);
      boundingBox.push_back(p3);
      boundingBox.push_back(p4);

      try
      {
         PolygonCutLibrary::Polygon polygon(polygonVertex);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary::Polygon polygon(polygonVertex);

      try
      {
         EXPECT_TRUE(abs(polygon.ComputeArea() - 15.37) < 1e-6);
         EXPECT_EQ(polygon.ComputeBoundingBox(), boundingBox);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_POLYGON_H
