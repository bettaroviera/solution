#ifndef __TEST_CUTPOLYGON_H
#define __TEST_CUTPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace PolygonTesting {

  TEST (TestPolygonCut, CutPolygonTest)
  {
      //lo provo per un poligono convesso

      Vector2d v1, v2, v3, v4;
      v1 << 1.0, 1.0;
      v2 << 5.0, 1.0;
      v3 << 5.0, 3.1;
      v4 << 1.0, 3.1;
      Vector2d s1, s2;
      s1 << 2.0, 1.2;
      s2 << 4.0, 3.0;

      //inserisco i vertici in un vettore di punti
      vector<Vector2d> vertex;
      vertex.push_back(v1);
      vertex.push_back(v2);
      vertex.push_back(v3);
      vertex.push_back(v4);

      try
      {
          PolygonCutLibrary::CutPolygon(vertex, s1, s2);
      }
      catch (const exception& exception)
      {
          FAIL();
      }

      PolygonCutLibrary::CutPolygon polygon(vertex, s1, s2); //creo un poligono

      try
      {
          polygon.Cut();
          EXPECT_EQ(2, polygon.NumIntersections());
      }
      catch (const exception& exception)
      {
          FAIL();
      }

      Vector2d inters1, inters2, a, b;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 1.77777777778, 1.0;
      inters2 << 4.11111111111, 3.1;

      try
      {
        // i punti di intersezione che calcola
        a = polygon.IntersectionPoints()[0];
        b = polygon.IntersectionPoints()[1];
        intersectionPoints = polygon.IntersectionPoints();
        // la loro differenza deve essere minore di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
            FAIL();
      }

      vector<Vector2d> newPoints;
      try
      {
        polygon.ChooseFunction(); //chiama al suo interno NewPolygonTwoInt, e quindi so che anche questa funziona, altrimenti non ritornerebbe i giusti poligoni tagliati
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v4);
      newVertex1.push_back(0);
      newVertex1.push_back(4);
      newVertex1.push_back(5);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(3);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v2);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s2);
      cuttedPolygon2.push_back(s1);
      newVertex2.push_back(4);
      newVertex2.push_back(1);
      newVertex2.push_back(2);
      newVertex2.push_back(7);
      newVertex2.push_back(6);
      newVertex2.push_back(5);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);

      try
      {
        EXPECT_EQ(allCuttedPol, polygon.CuttedPolygons());
        EXPECT_EQ(allNewVertex, polygon.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }


      //adesso controllo che funzioni anche per un poligono concavo
      Vector2d v5, v6;
      v1 << 1.5, 1.0;
      v2 << 5.6, 1.5;
      v3 << 5.5, 4.8;
      v4 << 4.0, 6.2;
      v5 << 3.2, 4.2;
      v6 << 1.0, 4.0;

      s1 << 2.0, 3.7;
      s2 << 4.1, 5.9;

      vector<Vector2d> concavePolygonVertex;
      concavePolygonVertex.push_back(v1);
      concavePolygonVertex.push_back(v2);
      concavePolygonVertex.push_back(v3);
      concavePolygonVertex.push_back(v4);
      concavePolygonVertex.push_back(v5);
      concavePolygonVertex.push_back(v6);

      try
      {
        PolygonCutLibrary::CutPolygon(concavePolygonVertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon concavePolygon(concavePolygonVertex, s1, s2);

      try
      {
        concavePolygon.Cut();
        EXPECT_EQ(4, concavePolygon.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters3, inters4, c, d;
      intersectionPoints.clear();
      // i punti di intersezione che mi aspetto
      inters1 << 4.2043269, 6.0092949;
      inters2 << 3.7213115, 5.5032787;
      inters3 << 2.4085973, 4.1280543;
      inters4 << 1.1912162, 2.8527027;

      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon.IntersectionPoints()[0];
        b = concavePolygon.IntersectionPoints()[1];
        c = concavePolygon.IntersectionPoints()[2];
        d = concavePolygon.IntersectionPoints()[3];
        intersectionPoints = concavePolygon.IntersectionPoints();
        //controllo che coincidano a meno di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
        EXPECT_TRUE(abs(c(0) - inters3(0)) < 1e-6);
        EXPECT_TRUE(abs(c(1) - inters3(1)) < 1e-6);
        EXPECT_TRUE(abs(d(0) - inters4(0)) < 1e-6);
        EXPECT_TRUE(abs(d(1) - inters4(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      newPoints.clear();
      try
      {
        concavePolygon.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon3;
      cuttedPolygon1.clear();
      cuttedPolygon2.clear();
      allCuttedPol.clear();
      vector<int> newVertex3;
      newVertex1.clear();
      newVertex2.clear();
      allNewVertex.clear();

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(v3);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v5);
      cuttedPolygon1.push_back(c);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(d);
      newVertex1.push_back(0);
      newVertex1.push_back(1);
      newVertex1.push_back(2);
      newVertex1.push_back(6);
      newVertex1.push_back(7);
      newVertex1.push_back(8);
      newVertex1.push_back(4);
      newVertex1.push_back(9);
      newVertex1.push_back(10);
      newVertex1.push_back(11);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s2);
      newVertex2.push_back(6);
      newVertex2.push_back(3);
      newVertex2.push_back(8);
      newVertex2.push_back(7);

      cuttedPolygon3.push_back(c);
      cuttedPolygon3.push_back(v6);
      cuttedPolygon3.push_back(d);
      cuttedPolygon3.push_back(s1);
      newVertex3.push_back(9);
      newVertex3.push_back(5);
      newVertex3.push_back(11);
      newVertex3.push_back(10);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allCuttedPol.push_back(cuttedPolygon3);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);
      allNewVertex.push_back(newVertex3);

      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }
  }
}

#endif // __TEST_POLYGON _H
