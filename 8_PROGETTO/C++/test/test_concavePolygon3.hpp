#ifndef __TEST_CONCAVEPOLYGON3_H
#define __TEST_CONCAVEPOLYGON3_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConcavePolygon3Testing {

  TEST(TestPolygonCut, TestConcavePolygon3)
  {
      Vector2d v1, v2, v3, v4, v5, v6, v7;
      v1 << 1.0, 1.0;
      v2 << 4.5, 0.5;
      v3 << 5.0, 4.5;
      v4 << 3.5, 3.5;
      v5 << 3.0, 5.0;
      v6 << 2.0, 3.5;
      v7 << 0.5, 5.5;

      Vector2d s1, s2;
      s1 << 1.1, 3.8;
      s2 << 2.9, 3.9;

      vector<Vector2d> concavePolygon3Vertex;
      concavePolygon3Vertex.push_back(v1);
      concavePolygon3Vertex.push_back(v2);
      concavePolygon3Vertex.push_back(v3);
      concavePolygon3Vertex.push_back(v4);
      concavePolygon3Vertex.push_back(v5);
      concavePolygon3Vertex.push_back(v6);
      concavePolygon3Vertex.push_back(v7);

      try
      {
        PolygonCutLibrary::CutPolygon(concavePolygon3Vertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon concavePolygon3(concavePolygon3Vertex, s1, s2);

      try
      {
        concavePolygon3.Cut();
        EXPECT_EQ(6, concavePolygon3.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, inters3, inters4, inters5, inters6, a, b, c, d, e, f;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 4.939161, 4.013287;
      inters2 << 4.209091, 3.972728;
      inters3 << 3.358182, 3.925454;
      inters4 << 2.242308, 3.863462;
      inters5 << 1.748, 3.836;
      inters6 << 0.691411, 3.777301;

      try
      {
        // i punti di intersezione che calcola
        a = concavePolygon3.IntersectionPoints()[0];
        b = concavePolygon3.IntersectionPoints()[1];
        c = concavePolygon3.IntersectionPoints()[2];
        d = concavePolygon3.IntersectionPoints()[3];
        e = concavePolygon3.IntersectionPoints()[4];
        f = concavePolygon3.IntersectionPoints()[5];
        intersectionPoints = concavePolygon3.IntersectionPoints();
        //controllo che coincidano a meno di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
        EXPECT_TRUE(abs(c(0) - inters3(0)) < 1e-6);
        EXPECT_TRUE(abs(c(1) - inters3(1)) < 1e-6);
        EXPECT_TRUE(abs(d(0) - inters4(0)) < 1e-6);
        EXPECT_TRUE(abs(d(1) - inters4(1)) < 1e-6);
        EXPECT_TRUE(abs(e(0) - inters5(0)) < 1e-6);
        EXPECT_TRUE(abs(e(1) - inters5(1)) < 1e-6);
        EXPECT_TRUE(abs(f(0) - inters6(0)) < 1e-6);
        EXPECT_TRUE(abs(f(1) - inters6(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
         PolygonCutLibrary::NewPoints(concavePolygon3Vertex, intersectionPoints, s1, s2);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: NewPoints newP(concavePolygon3Vertex, intersectionPoints, s1, s2);

      vector<Vector2d> newPoints;
      try
      {
        newP.CalcPoints();
        EXPECT_EQ(15, newP.NumNewPoints());
        newPoints = newP.ReturnPoints();
        // controllo che i primi 7 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        EXPECT_EQ(v5, newPoints[4]);
        EXPECT_EQ(v6, newPoints[5]);
        EXPECT_EQ(v7, newPoints[6]);
        // controllo che 6 siano le intersezioni
        EXPECT_EQ(newPoints[7], a);
        EXPECT_EQ(newPoints[8], b);
        EXPECT_EQ(newPoints[9], c);
        EXPECT_EQ(newPoints[10], d);
        EXPECT_EQ(newPoints[11], e);
        EXPECT_EQ(newPoints[12], f);
        // controllo che gli ultimi due siano gli estremi del segmento
        EXPECT_EQ(newPoints[13], s1);
        EXPECT_EQ(newPoints[14], s2);
        concavePolygon3.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2, cuttedPolygon3, cuttedPolygon4;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2, newVertex3, newVertex4;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(b);
      cuttedPolygon1.push_back(v4);
      cuttedPolygon1.push_back(c);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(d);
      cuttedPolygon1.push_back(v6);
      cuttedPolygon1.push_back(e);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(f);
      newVertex1.push_back(0);
      newVertex1.push_back(1);
      newVertex1.push_back(7);
      newVertex1.push_back(8);
      newVertex1.push_back(3);
      newVertex1.push_back(9);
      newVertex1.push_back(10);
      newVertex1.push_back(11);
      newVertex1.push_back(5);
      newVertex1.push_back(12);
      newVertex1.push_back(13);
      newVertex1.push_back(14);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(b);
      newVertex2.push_back(7);
      newVertex2.push_back(2);
      newVertex2.push_back(8);

      cuttedPolygon3.push_back(c);
      cuttedPolygon3.push_back(v5);
      cuttedPolygon3.push_back(d);
      cuttedPolygon3.push_back(s2);
      newVertex3.push_back(9);
      newVertex3.push_back(4);
      newVertex3.push_back(11);
      newVertex3.push_back(10);

      cuttedPolygon4.push_back(e);
      cuttedPolygon4.push_back(v7);
      cuttedPolygon4.push_back(f);
      cuttedPolygon4.push_back(s1);
      newVertex4.push_back(12);
      newVertex4.push_back(6);
      newVertex4.push_back(14);
      newVertex4.push_back(13);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allCuttedPol.push_back(cuttedPolygon3);
      allCuttedPol.push_back(cuttedPolygon4);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);
      allNewVertex.push_back(newVertex3);
      allNewVertex.push_back(newVertex4);


      try
      {
      EXPECT_EQ(allCuttedPol, concavePolygon3.CuttedPolygons());
      EXPECT_EQ(allNewVertex, concavePolygon3.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_CONCAVEPOLYGON3_H
