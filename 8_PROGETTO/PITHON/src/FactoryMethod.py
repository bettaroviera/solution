from abc import ABC


class FactoryMethod(ABC):  # Abstract class, create Result
    def produce_new_points(self) -> None:
        pass

    def produce_new_polygons(self) -> None:
        pass
