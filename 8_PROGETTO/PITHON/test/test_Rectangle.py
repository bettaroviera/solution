from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestRectangle(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(1, 1),
                    V2d.Vector2d(5, 1),
                    V2d.Vector2d(5, 3.1),
                    V2d.Vector2d(1, 3.1)]
        return vertices

    def test_Polygon_Ret(self):
        try:
            vertices = TestRectangle.FillPolygonVertices()
            s1 = V2d.Vector2d(2, 1.2)
            s2 = V2d.Vector2d(4, 3)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n[(1.0, 1.0), "
                             "(5.0, 1.0), (5.0, 3.1), (1.0, 3.1), (2.0, 1.2), (4.0, 3.0), (1.7778, 1.0), "
                             "(4.1111, 3.1)]\n"
                             "The number of new points is:\n"
                             "8\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.0, 1.0), (1.7778, 1.0), (2.0, 1.2), (4.0, 3.0), (4.1111, 3.1), (1.0, 3.1)], "
                             "[(1.7778, 1.0), (5.0, 1.0), (5.0, 3.1), (4.1111, 3.1), (4.0, 3.0), (2.0, 1.2)])\n"
                             "The numerical order is:\n"
                             "([0, 4, 5, 6, 7, 3], [4, 1, 2, 7, 6, 5])")
        except:
            self.fail()
