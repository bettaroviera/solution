from unittest import TestCase
import src.Vector2D as V2d


class TestSegment(TestCase):

    def test_X(self):
        try:
            A = V2d.Vector2d(1, 2)
            self.assertEqual(V2d.Vector2d.x(A), 1)
        except:
            self.fail()

    def test_Y(self):
        try:
            A = V2d.Vector2d(1, 2)
            self.assertEqual(V2d.Vector2d.y(A), 2)
        except:
            self.fail()

    def test_into_matrix(self):
        try:
            A = V2d.Vector2d(1, 2)
            self.assertEqual(str(V2d.Vector2d.into_matrix(A)), "[[1.]\n [2.]]")
        except:
            self.fail()
