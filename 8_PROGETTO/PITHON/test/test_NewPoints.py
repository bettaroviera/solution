from unittest import TestCase
import numpy as np
import src.NewPoints as Np
import src.Vector2D as V2d


class TestPoint(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(2, -2),
                    V2d.Vector2d(0, -1),
                    V2d.Vector2d(3, 1),
                    V2d.Vector2d(0, 2),
                    V2d.Vector2d(3, 2),
                    V2d.Vector2d(3, 3),
                    V2d.Vector2d(-1, 3),
                    V2d.Vector2d(-3, 1),
                    V2d.Vector2d(0, 0),
                    V2d.Vector2d(-3, -2)]
        return vertices

    def test_order_vertex(self):
        try:
            vertices = TestPoint.FillPolygonVertices()
            s1 = V2d.Vector2d(-1, -1)
            s2 = V2d.Vector2d(1, 1)
            intersection_points = np.array([[-2., 1.5, 2., 3., 0.], [-2., 1.5, 2., 3., 0.]])
            new_points = Np.NewPoints(vertices, intersection_points, s1, s2)
            self.assertEqual(str(Np.NewPoints.ordered_vertex(new_points)),
                             "[[-3.  2.  0.  3.  0.  3.  3. -1. -3.  0.]\n [-2. -2. -1.  1.  2.  2.  3.  3.  1.  0.]]")
        except:
            self.fail()

    def test_new_points(self):
        try:
            vertices = TestPoint.FillPolygonVertices()
            intersection_points = np.array([[-2., 1.5, 2., 3., 0.], [-2., 1.5, 2., 3., 0.]])
            s1 = V2d.Vector2d(-1, -1)
            s2 = V2d.Vector2d(1, 1)
            new_points = Np.NewPoints(vertices, intersection_points, s1, s2)
            self.assertEqual(str(Np.NewPoints.calc_points(new_points)),
                             "[(-3.0, -2.0), (2.0, -2.0), (0.0, -1.0), (3.0, 1.0), (0.0, 2.0), (3.0, 2.0), (3.0, 3.0), "
                             "(-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0), (-1.0, -1.0), (1.0, 1.0), (-2.0, -2.0), "
                             "(1.5, 1.5), (2.0, 2.0)]")
        except:
            self.fail()

    def test_result(self):
        try:
            vertices = TestPoint.FillPolygonVertices()
            intersection_points = np.array([[-2., 1.5, 2., 3., 0.], [-2., 1.5, 2., 3., 0.]])
            s1 = V2d.Vector2d(-1, -1)
            s2 = V2d.Vector2d(1, 1)
            new_points = Np.NewPoints(vertices, intersection_points, s1, s2)
            new_points = Np.NewPoints.calc_points(new_points)
            self.assertEqual(Np.NewPoints.return_points(new_points),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(-3.0, -2.0), (2.0, -2.0), (0.0, -1.0), (3.0, 1.0), (0.0, 2.0), (3.0, 2.0), (3.0, 3.0), "
                             "(-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0), (-1.0, -1.0), (1.0, 1.0), (-2.0, -2.0), "
                             "(1.5, 1.5), (2.0, 2.0)]\nThe number of new points is:\n15")
        except:
            self.fail()
