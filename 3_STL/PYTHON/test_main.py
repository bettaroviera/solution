from unittest import TestCase
import main as pizza_library


class TestPizzeria(TestCase):
    def test_ingredient(self):
        pizzeria = pizza_library.Pizzeria()

        try:
            pizzeria.add_ingredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.add_ingredient("Mozzarella", "Traditionally southern Italian cheese", 3)
        except Exception as ex:
            self.fail()

        try:
            pizzeria.add_ingredient("Mozzarella", "Traditionally southern Italian cheese", 3)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Ingredient already inserted")

        try:
            ingredient = pizzeria.find_ingredient("Mozzarella")
            self.assertEqual(ingredient.name, "Mozzarella")
            self.assertEqual(ingredient.description, "Traditionally southern Italian cheese")
            self.assertEqual(ingredient.price, 3)
        except Exception as ex:
            self.fail()

        try:
            ingredient = pizzeria.find_ingredient("Eggplant")
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Ingredient not found")

        try:
            self.assertEqual(pizzeria.list_ingredients(),
                             "Mozzarella - 'Traditionally southern Italian cheese': 3 euro\nTomato - 'Red berry of "
                             "the plant Solanum lycopersicum': 2 euro\n")
        except Exception as ex:
            self.fail()

    def test_pizza(self):
        pizzeria = pizza_library.Pizzeria()

        try:
            pizzeria.add_ingredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.add_ingredient("Mozzarella", "Traditionally southern Italian cheese", 3)

            pizzeria.add_pizza("Margherita", ["Tomato", "Mozzarella"])
            pizzeria.add_pizza("Marinara", ["Tomato"])
        except Exception as ex:
            self.fail()

        try:
            pizzeria.add_pizza("Margherita", ["Tomato", "Mozzarella"])
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Pizza already inserted")

        try:
            pizza = pizzeria.find_pizza("Margherita")
            self.assertEqual(pizza.name, "Margherita")
            self.assertEqual(pizza.num_ingredients(), 2)
            self.assertEqual(pizza.compute_price(), 5)
        except Exception as ex:
            self.fail()

        try:
            pizza = pizzeria.find_pizza("Prosciutto")
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Pizza not found")

        try:
            self.assertEqual(pizzeria.menu(), "Margherita (2 ingredients): 5 euro\nMarinara (1 ingredients): 2 euro\n")
        except Exception as ex:
            self.fail()

    def test_order(self):
        pizzeria = pizza_library.Pizzeria()

        try:
            pizzeria.add_ingredient("Tomato", "Red berry of the plant Solanum lycopersicum", 2)
            pizzeria.add_ingredient("Mozzarella", "Traditionally southern Italian cheese", 3)

            pizzeria.add_pizza("Margherita", {"Tomato", "Mozzarella"})
            pizzeria.add_pizza("Marinara", {"Tomato"})

            self.assertEqual(pizzeria.create_order(["Margherita", "Marinara"]), 1000)
        except Exception as ex:
            self.fail()

        try:
            pizzeria.create_order({})
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Empty order")

        try:
            order = pizzeria.find_order(1000)
            self.assertEqual(order.num_pizzas(), 2)
            self.assertEqual(order.compute_total(), 7)
        except Exception as ex:
            self.fail()

        try:
            order = pizzeria.find_order(1001)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Order not found")

        try:
            order = pizzeria.get_receipt(1001)
            self.fail()
        except Exception as ex:
            self.assertEqual(str(ex), "Order not found")

        try:
            self.assertTrue(pizzeria.get_receipt(1000) == "- Margherita, 5 euro\n- Marinara, 2 euro\n  TOTAL: 7 euro\n")
        except Exception as ex:
            self.fail()








