class Ingredient:
    def __init__(self, name, price, description):
        self.name = name
        self.price = price
        self.description = description


class Pizza:
    def __init__(self, name):
        self.name = name
        self.ingredients_pizza = []  # list

    def add_ingredient(self, ingredient: Ingredient):
        self.ingredients_pizza.append(ingredient)

    def num_ingredients(self) -> int:
        return len(self.ingredients_pizza)

    def compute_price(self) -> int:
        total_price = 0
        for ingredient in self.ingredients_pizza:  # scorro gli ingredienti
            total_price += ingredient.price
        return total_price


class Order:
    def __init__(self):
        self.pizzas_order = []  # list
        self.num_order = 0

    def num_pizzas(self):
        return len(self.pizzas_order)

    def add_pizza(self, pizza: Pizza):
        self.pizzas_order.append(pizza)

    def get_pizza(self, position) -> Pizza:
        if 0 < position < self.num_pizzas():  # check sulla posizione passata
            return self.pizzas_order[position - 1]
        else:
            raise ValueError("Position passed is wrong")

    def initialize_order(self, num_pizzas: int):
        self.pizzas_order = [0] * num_pizzas  # dichiaro la lunghezza

    def compute_total(self) -> int:
        total_price = 0
        for pizza in self.pizzas_order:  # scorro le pizze nell'ordine
            total_price += pizza.compute_price()
        return total_price


class Pizzeria:
    def __init__(self):
        self.ordered_ingredients = []  # list
        self.ingredients_in_kitchen = []  # list
        self.pizzas_in_menu = []  # list
        self.orders = []  # list

    def add_ingredient(self, name, description, price):
        for ingredient in self.ingredients_in_kitchen:  # scorro gli ingredienti
            if ingredient.name == name:
                raise ValueError("Ingredient already inserted")
        new_ingredients = Ingredient(name, price, description)  # creo un'istanza di Ingredient
        self.ingredients_in_kitchen.append(new_ingredients)

    def find_ingredient(self, name) -> Ingredient:
        for ingredient in self.ingredients_in_kitchen:
            if ingredient.name == name:
                return ingredient
        raise ValueError("Ingredient not found")

    def add_pizza(self, name, ingredients: []):
        for pizza in self.pizzas_in_menu:  # scorro le pizze
            if pizza.name == name:
                raise ValueError("Pizza already inserted")
        new_pizza = Pizza(name)
        # per aggiungere gli ingredienti scorro la lista di input e la confronto con gli elementi della cucina
        for ing in ingredients:
            for ingredient in self.ingredients_in_kitchen:
                if ing == ingredient.name:
                    new_pizza.ingredients_pizza.append(ingredient)
        self.pizzas_in_menu.append(new_pizza)

    def find_pizza(self, name: str) -> Pizza:
        for pizza in self.pizzas_in_menu:
            if pizza.name == name:
                return pizza
        raise ValueError("Pizza not found")

    def create_order(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise ValueError("Empty order")
        new_order = Order()
        for i in range(0, len(pizzas)):
            for pizza in self.pizzas_in_menu:
                if pizzas[i] == pizza.name:
                    new_order.pizzas_order.append(pizza)
        new_order.num_order = 1000 + len(self.orders)
        self.orders.append(new_order)
        return new_order.num_order

    def find_order(self, num_order: int) -> Order:
        for order in self.orders:
            if order.num_order == num_order:
                return order
        raise ValueError("Order not found")

    def get_receipt(self, num_order: int) -> str:
        for order in self.orders:
            if order.num_order == num_order:  # cerco il numero di ordine corretto
                receipt = ""  # str
                for pizza in order.pizzas_order:  # formo lo scontrino
                    receipt += "- " + pizza.name + ", " + str(pizza.compute_price()) + " euro\n"
                receipt += "  TOTAL: " + str(order.compute_total()) + " euro\n"
                return receipt
        raise ValueError("Order not found")

    def list_ingredients(self) -> str:
        list_of_ingredients = ""  # str
        flag = []
        for ingredients in self.ingredients_in_kitchen:
            flag.append(ingredients.name)
        flag.sort()  # riordino alfabeticamente
        for t in flag:
            for ingredients in self.ingredients_in_kitchen:
                if t == ingredients.name:
                    self.ordered_ingredients.append(ingredients)
        for i in range(0, len(self.ordered_ingredients)):  # formo la lista degli ingredienti
            list_of_ingredients += self.ordered_ingredients[i].name + " - '" + self.ordered_ingredients[i].description \
                                   + "': " + str(self.ordered_ingredients[i].price) + " euro\n"
        return list_of_ingredients

    def menu(self) -> str:
        menu = ""
        for pizza in self.pizzas_in_menu:  # scorro le pizze nel menu
            menu += pizza.name + " (" + str(pizza.num_ingredients()) + " ingredients): " + \
                    str(pizza.compute_price()) + " euro\n"
        return menu
