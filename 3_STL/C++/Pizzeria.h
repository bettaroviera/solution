#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_set>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
      int m_iData;
      bool operator<(const Ingredient &rhs) const {return m_iData < rhs.m_iData;}

  };

  class Pizza {
    public:
      string Name;
      vector <Ingredient> IngredinetsPizza;

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const;
      int ComputePrice() const;
  };

  class Order {
    public:
      vector <Pizza> pizzasOrder;
      unsigned int numOrder;

      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const;
  };

  class Pizzeria {
    public:
      vector <Ingredient> ingredientsInKitchen;
      vector <Pizza> pizzasInMenu;
      vector <Order> orders;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients();
      string Menu() const;
  };

};

#endif // PIZZERIA_H
