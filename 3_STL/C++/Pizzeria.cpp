#include "Pizzeria.h"

namespace PizzeriaLibrary
{

  void Pizza::AddIngredient(const Ingredient &ingredient)
  {
    IngredinetsPizza.push_back(ingredient);
  }

  int Pizza::NumIngredients() const
  {
    return IngredinetsPizza.size();
  }

  int Pizza::ComputePrice() const
  {
    int totalPrice = 0;
    for (int i = 0; i < NumIngredients(); i++)
        totalPrice = totalPrice + IngredinetsPizza[i].Price;
    return totalPrice;
  }

  void Order::InitializeOrder(int numPizzas)
  {
    pizzasOrder.reserve(numPizzas);
  }

  void Order::AddPizza(const Pizza &pizza)
  {
    pizzasOrder.push_back(pizza);
  }

  int Order::NumPizzas() const
  {
    return pizzasOrder.size();
  }

  const Pizza &Order::GetPizza(const int &position) const
  {
    if (0 < position < (NumPizzas() + 1))
      return pizzasOrder[position -1];
    else
        throw runtime_error("Position passed is wrong");
  }

  int Order::ComputeTotal() const
  {
    int totalPrice = 0;
    for (int i = 0; i < NumPizzas() ;i++)
      totalPrice = totalPrice + pizzasOrder[i].ComputePrice();
    return totalPrice;
  }

  void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
  {
    for (unsigned int i = 0; i < ingredientsInKitchen.size(); i++)
      {
        if (ingredientsInKitchen[i].Name == name)
            throw runtime_error("Ingredient already inserted");
      }
    Ingredient newIngredients;
    newIngredients.Name = name;
    newIngredients.Description = description;
    newIngredients.Price = price;
    ingredientsInKitchen.push_back(newIngredients);
    //std::sort(ingredientsInKitchen.begin(), ingredientsInKitchen.end());
  }

  const Ingredient &Pizzeria::FindIngredient(const string &name) const
  {
    for (unsigned int i = 0; i < ingredientsInKitchen.size(); i++)
      {
        if (ingredientsInKitchen[i].Name == name)
          return ingredientsInKitchen[i];
      }
    throw runtime_error("Ingredient not found");

  }

  void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
  {
    for (unsigned int i = 0; i < pizzasInMenu.size(); i++)
      {
        if (pizzasInMenu[i].Name == name)
          throw runtime_error("Pizza already inserted");
      }
    Pizza newPizza;
    newPizza.Name = name;
    for (unsigned int i = 0; i < ingredients.size(); i++)
      {
        for (unsigned int j = 0; j < ingredientsInKitchen.size(); j++)
          {
            if (ingredients[i] == ingredientsInKitchen[j].Name)
                newPizza.IngredinetsPizza.push_back(ingredientsInKitchen[j]);
          }
      }
    pizzasInMenu.push_back(newPizza);
  }

  const Pizza &Pizzeria::FindPizza(const string &name) const
  {
    for (unsigned int i = 0; i < pizzasInMenu.size(); i++)
      {
        if (pizzasInMenu[i].Name == name)
          return pizzasInMenu[i];
      }
    throw runtime_error("Pizza not found");
  }

  int Pizzeria::CreateOrder(const vector<string> &pizzas)
  {
    Order newOrder;
    if (pizzas.size() == 0)
      throw runtime_error("Empty order");

    for (unsigned int i = 0; i < pizzas.size(); i++)
      {
        for (unsigned int j = 0; j < pizzasInMenu.size(); j++)
          {
            if (pizzas[i] == pizzasInMenu[j].Name)
                newOrder.pizzasOrder.push_back(pizzasInMenu[j]);
          }
      }
    newOrder.numOrder = 1000 + orders.size();
    orders.push_back(newOrder);
    return newOrder.numOrder;
  }

  const Order &Pizzeria::FindOrder(const int &numOrder) const
  {
    Order newOrder;
    for (unsigned int i = 0; i < orders.size(); i++)
      {
        if (orders[i].numOrder == unsigned(numOrder))
          return orders[i];
      }
    throw runtime_error("Order not found");
  }

  string Pizzeria::GetReceipt(const int &numOrder) const
  {
    Order newOrder;
    string receipt;
    for (unsigned int i = 0; i < orders.size(); i++)
      {
        if (orders[i].numOrder == unsigned(numOrder))
          {
            for (unsigned int j = 0; j < unsigned(orders[i].NumPizzas()); j++)
              {
                receipt = receipt + "- " + orders[i].pizzasOrder[j].Name + ", " + to_string(orders[i].pizzasOrder[j].ComputePrice()) + " euro\n";
              }
            receipt = receipt + "  TOTAL: " + to_string(orders[i].ComputeTotal()) + " euro\n";
            return receipt;
          }
      }
     throw runtime_error("Order not found");
  }

  string Pizzeria::ListIngredients()
  {
    vector<string> flag;
    vector<Ingredient> orderedIngredients;
    for (unsigned int i = 0; i < ingredientsInKitchen.size(); i++)
      {
        flag.push_back(ingredientsInKitchen[i].Name);
      }
    std::sort(flag.begin(), flag.end());
    for (unsigned int i = 0; i < flag.size(); i++)
      {
        for (unsigned int j = 0; j < ingredientsInKitchen.size(); j++)
          {
            if (flag[i] == ingredientsInKitchen[j].Name)
              orderedIngredients.push_back(ingredientsInKitchen[j]);
          }
      }
    string listOfIngredients;
    for (unsigned int i = 0; i < orderedIngredients.size(); i++)
        listOfIngredients += orderedIngredients[i].Name + " - '" + orderedIngredients[i].Description + "': " + to_string(orderedIngredients[i].Price) + " euro\n";

    return listOfIngredients;
  }

  string Pizzeria::Menu() const
  {
    string menu;
    for (int i = (pizzasInMenu.size() - 1); i>-1; i--)
        menu += pizzasInMenu[i].Name + " (" + to_string(pizzasInMenu[i].NumIngredients()) + " ingredients): " + to_string(pizzasInMenu[i].ComputePrice()) + " euro\n";
    return menu;
  }

}
